# siv

Ein einfacher Bildbetrachter

Besonderheit von siv: ändert sich das Bild, wird es automatisch neu geladen (verwendet inotify).

## Starten

* Lade den Quellcode herunter: `git clone https://gitlab.com/dknof/siv.git`
* Wechsel in das src-Verzeichnis: `cd src`
* Kompiliere siv: `make`
* Starte siv: `./siv [Bilddatei]`

### Voraussetzungen

Du benötigst make und einen C++-Compiler mit C++14-Unterstützung
Die benutzte Bibliothek ist:

* [gtkmm3](https://www.gtkmm.org)

Debian:

    apt install g++ make
    apt install libgtkmm-3.0-dev

## Aufruf

Im src-Unterverzeichnis make aufrufen.
Es wird gtkmm Version 3 benötigt (debian-Paket: libgtkmm-3.0-dev).

Optionen:
```
-h -? --hilfe
        Gibt diese Hilfe aus
-v --version
        Gibt die Version aus
-L --lizenz
        Gibt die Lizenz (GPLv3) aus
Dateipfad
        Dateien zum Laden
```

Wird als Dateipfad ein Verzeichnis angegeben, wird immer das neuste Bild
aus dem Verzeichnis angezeigt. Bilder werden über die Dateiendung (fest
einprogrammiert) erkannt. Achtung: Das neuste Bild wird über den Zeitstempel
der letzten Änderung identifiziert, es ist nicht notwendigerweise das Bild,
das als letztes in das Verzeichnis kopiert wurde. Bei Bedarf ist „touch“
zu verwenden.

== Bedienung
```
Mausrad: verkleinern/vergrößern
1: Originalgröße
2-9: Bildgröße 1/2 bis 1/9
0: Bildgröße passend auf den Bildschirm (Skalierung immer 1/n-tel)
,: verkleinern
.: vergrößern
f: Vollbild/Fenstermodus
r: Bild neu laden (sollte nicht nötig sein)
Esc/q/x: beenden
?/h: Hilfe-Fenster anzeigen
a: Über-Fenster anzeigen
```

## Website

Der Quelltext befindet sich unter <https://gitlab.com/dknof/siv>.

## Author

* Dr. Diether Knof <dknof+siv@posteo.de>

## Mitwirken

Siv ist ein freies Projekt. Bei Interesse kontaktiere den [author](mailto:dknof+siv@posteo.de).

## Lizenz

Siv ist Freie Software: Sie können es unter den Bedingungen
der GNU General Public License, wie von der Free Software Foundation,
Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
veröffentlichten Version, weiter verteilen und/oder modifizieren.

## Status des Projekts

Dieses Projekt ist abgeschlossen (Stand: 10. Oktober 2019).
