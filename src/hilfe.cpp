#include "constants.h"
#include "hilfe.h"

unique_ptr<Hilfe> hilfe;

/**
 ** Konstruktor
 ** 
 ** @param     -
 **
 ** @return    -
 **/
Hilfe::Hilfe() :
  Gtk::MessageDialog("Hilfe")
{
  this->signal_realize().connect(sigc::mem_fun(*this, &Hilfe::init));
  this->signal_response().connect(sigc::hide(sigc::mem_fun(*this, &Hilfe::hide)));
} // Hilfe::Hilfe()

/**
 ** initializiere das Fenster
 ** 
 ** @param     -
 **
 ** @return    -
 **/
void
Hilfe::init()
{
  this->set_title("siv – Hilfe");
  this->set_message("Hilfe");
#include "hilfe.string"
  this->set_secondary_text(hilfe_string);

  return ;
} // Hilfe::init()
