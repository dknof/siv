#include "constants.h"
#include "bild.h"
#include "hilfe.h"
#include "ueber.h"

#include <gtkmm/image.h>
#include <gtkmm.h>

Bild::Bild(Pfad const pfad) :
  Gtk::Window(),
  pfad(pfad)
{
  this->image = Gtk::manage(new Gtk::DrawingArea());
  this->add(*this->image);
  this->image->show();
  this->reload();
  this->signal_draw().connect(sigc::mem_fun(*this, &Bild::draw));
  this->add_events(Gdk::SCROLL_MASK);
} // Bild::Bild()

/** Aktualisiere die Anzeige, wenn sie veraltet ist
 **/
void
Bild::update_if_outdated()
{
  if (this->outdated)
    this->update();
} // void Bild::update_if_outdated()

/** Aktualisiere die Anzeige
 **/
void
Bild::update()
{
  if (!this->get_realized())
    return ;
  int width, height;
  this->get_size(width, height);
  this->draw(this->image->get_window()->create_cairo_context());
  this->outdated = false;
} // void Bild::update()

/** Aktualisiere die Anzeige
 **
 ** @param    cr   cairo context
 **
 ** @return   true
 **/
bool
Bild::draw(Cairo::RefPtr<::Cairo::Context> cr)
{
  cr->scale(this->scale, this->scale);
  Gdk::Cairo::set_source_pixbuf(cr, pixbuf, 0, 0);
  cr->paint();
  return true;
} // bool Bild::draw(Cairo::RefPtr<::Cairo::Context> cr)

/** Entfernt das Bild
 **/
void
Bild::clear()
{
  this->pixbuf.clear();
}

/** Lädt das Bild neu
 **/
void
Bild::reload()
{
  auto const pfad = this->pfad.neustes_bild();
  if (!pfad) {
    throw "Konnte „" + this->pfad + "“ nicht aktualisieren.";
  }
  this->set_title(pfad.datei());
  try {
    this->pixbuf = Gdk::Pixbuf::create_from_file(pfad);
    if (!this->pixbuf)
      throw ;
    double const width = this->pixbuf->get_width() * scale;
    double const height = this->pixbuf->get_height() * scale;
    if (   (this->get_screen()->get_width() < width)
        || (this->get_screen()->get_height() < height)) {
      this->auto_scale_fit_screen();
    } else {
      int w, h;
      this->get_size(w, h);
      if (!(   (w-1 < width) && (width < w+1)
            && (h-1 < height) && (height < h+1)) ) {
        this->resize(round(width), round(height));
      }
    }
    this->set_window_restrictions();
  } catch (...) {
    cerr << "Das Bild " + pfad + " konnte nicht neu geladen werden.\n" << '\n';
    throw Glib::FileError(Glib::FileError::FAILED,
                          "Das Bild " + pfad + " konnte nicht neu geladen werden.\n");
  }
  this->set_scale(this->scale);
  this->update();
} // void Bild::reload()

/** Ändert die Skalierung entsprechend der Fenstergröße
 **/
void
Bild::auto_scale()
{
  if (!this->pixbuf)
    return ;
  int width, height;
  this->get_size(width, height);
  auto const scale = min(width / static_cast<double>(this->pixbuf->get_width()),
                         height / static_cast<double>(this->pixbuf->get_height()));
  this->set_scale(scale);
} // void Bild::auto_scale()

/** Ändert die Skalierung, so dass das Bild in den Bildschirm passt
 **/
void
Bild::auto_scale_fit_screen()
{
  auto screen = this->get_screen();
  if (!screen) {
    cerr << "Screen nicht gesetzt\n";
    return ;
  }
  int const faktor = min(this->pixbuf->get_width() / (this->get_screen()->get_width() / 2),
                         this->pixbuf->get_height() / (this->get_screen()->get_height() / 2));
  this->set_scale(faktor > 1 ? 1.0 / faktor : 1, true);
} // void Bild::auto_scale_fit_screen()

/** setzt die Skalierung
 **
 ** @param    scale    neue Skalierung
 ** @param    resize   ob die Größe des Fensters geändert werden soll
 **/
void
Bild::set_scale(double const scale, bool const resize)
{
  double const width = this->pixbuf->get_width() * scale;
  double const height = this->pixbuf->get_height() * scale;
  if (   resize
      && (   !this->get_realized()
          || !(this->get_window()->get_state() & Gdk::WINDOW_STATE_FULLSCREEN))) {
    int w, h;
    this->get_size(w, h);
    if (!(   (w-1 < width) && (width < w+1)
          && (h-1 < height) && (height < h+1)) ) {
      this->resize(round(width), round(height));
    }
  }
  this->scale = min(width / this->pixbuf->get_width(),
                    height / this->pixbuf->get_height());
  this->outdated = true;
} // void Bild::set_scale(double scale, bool resize = false)

/** Gibt die neuste Datei zurück
 **
 ** @param    -
 **
 ** @return   Ist pfad eine Datei, wird sie zurückgegeben, ist pfad ein Verzeichnis wird das neuste Bild zurückgegeben
 **/
Pfad
Bild::neustes_bild() const
{
  if (this->pfad.ist_datei())
    return this->pfad;
  if (this->pfad.ist_verzeichnis())
    return this->pfad.neustes_bild();
  throw "Bild „" + this->pfad + "“ ist weder eine Datei noch ein Verzeichnis.";
} // string Bild::neustes_bild() const

/** Ereignis: Tastendruck
 **
 ** @param    key   gedrückte Taste
 **
 ** @return   ob der Tastendruck verarbeitet wurde
 **/
bool
Bild::on_key_press_event(GdkEventKey* const key)
{
  //cout << "Taste (Bild): " << gdk_keyval_name(key->keyval) << '\n';
  switch (key->keyval) {
  case GDK_KEY_Escape: // Fenster schließen
  case GDK_KEY_q: // Fenster schließen
  case GDK_KEY_x: // Fenster schließen
    this->hide();
    return true;
  case GDK_KEY_question: // Hilfe
  case GDK_KEY_h: // Hilfe
    if (!hilfe)
      hilfe = make_unique<Hilfe>();
    hilfe->present();
    return true;
  case GDK_KEY_a: // Über
    if (!ueber)
      ueber = make_unique<Ueber>();
    ueber->present();
    return true;
  case GDK_KEY_r: // Bild neu laden
    try {
      this->reload();
    } catch (...) {
      this->hide();
    }
    return true;
  case GDK_KEY_f: // Vollbild/Fenster
    if (this->get_window()->get_state() & Gdk::WINDOW_STATE_FULLSCREEN)
      this->unfullscreen();
    else
      this->fullscreen();
    this->auto_scale();
    return true;
  case GDK_KEY_1: // Originalgröße
    this->set_scale(1, true);
    return true;
  case GDK_KEY_2: // Größe 1/2
    this->set_scale(1.0 / 2, true);
    return true;
  case GDK_KEY_3: // Größe 1/3
    this->set_scale(1.0 / 3, true);
    return true;
  case GDK_KEY_4: // Größe 1/4
    this->set_scale(1.0 / 4, true);
    return true;
  case GDK_KEY_5: // Größe 1/5
    this->set_scale(1.0 / 5, true);
    return true;
  case GDK_KEY_6: // Größe 1/6
    this->set_scale(1.0 / 6, true);
    return true;
  case GDK_KEY_7: // Größe 1/7
    this->set_scale(1.0 / 7, true);
    return true;
  case GDK_KEY_8: // Größe 1/8
    this->set_scale(1.0 / 8, true);
    return true;
  case GDK_KEY_9: // Größe 1/9
    this->set_scale(1.0 / 9, true);
    return true;
  case GDK_KEY_0: // Größe 1, maximal Bildschirmgröße
    this->auto_scale_fit_screen();
    return true;
  case GDK_KEY_period: // vergrößern
    this->set_scale(this->scale / 0.9, true);
    return true;
  case GDK_KEY_comma: // verkleinern
    this->set_scale(this->scale * 0.9, true);
    return true;
    // Pfeiltasten: Bildausschnitt verschieben
    // Strg-Num+: im Uhrzeigersinn rotieren (90 Grad)
    // Strg-Num-: gegen den Uhrzeigersinn rotieren (90 Grad)
    // Umschalt-Num+: im Uhrzeigersinn rotieren (1 Grad)
    // Umschalt-Num-: im Uhrzeigersinn rotieren (1 Grad)
  } // switch (key->keyval)

  return false;
} // bool Bild::on_key_press_event(GdkEventKey* key)

/** Ereignis: Scrollen (Mausrad)
 **
 ** @param    scroll   Scroll-Ereignis
 **
 ** @return   ob das Scroll-Ereignis verarbeitet wurde
 **/
bool
Bild::on_scroll_event(GdkEventScroll* const scroll)
{
  switch (scroll->direction) {
  case GDK_SCROLL_UP:
    this->set_scale(this->scale / 0.9, true);
    return true;
  case GDK_SCROLL_DOWN:
    this->set_scale(this->scale * 0.9, true);
    return true;
  default:
    break;
  } // switch (scroll->direction)
  return false;
} // bool Bild::on_scroll_event(GdkEventScroll* scroll)

/** Ereignis: Bildgröße verändert
 **
 ** @param     event    Ereignis-Daten
 **
 ** @return    true (ob das Ereignis verarbeitet wurde)
 **/
bool
Bild::on_configure_event(GdkEventConfigure* event)
{
  int width, height;
  this->get_size(width, height);
  bool const size_changed = ((width == event->width) || (height == event->height));
  this->Window::on_configure_event(event);
  if (!size_changed)
    return false;
  this->auto_scale();
  return false;
} // bool Bild::on_configure_event(GdkEventConfigure* event)

/** Setzt maximale Fenstergröße und Seitenverhältnis, wenn das Fenster im Fenstermodus ist, entfernt die Einschränkungen, wenn es im Vollbild-Modus ist.
 **/
void
Bild::set_window_restrictions()
{
  Gdk::Geometry geometry;
#if 0
  if (this->get_realized()
      && (this->get_window()->get_state() & Gdk::WINDOW_STATE_FULLSCREEN)) {
    this->set_geometry_hints(*this, geometry, static_cast<Gdk::WindowHints>(0));
  } else { // Fenstermodus
#endif
    geometry.max_width = this->pixbuf->get_width();
    geometry.max_height = this->pixbuf->get_height();
    geometry.min_aspect = (this->pixbuf->get_width() /
                           static_cast<double>(this->pixbuf->get_height()));
    geometry.max_aspect = geometry.min_aspect;
    this->set_geometry_hints(*this, geometry,
                             Gdk::HINT_ASPECT);
    // MAX_SIZE funktioniert nicht mit Vollbild
    //Gdk::HINT_MAX_SIZE | Gdk::HINT_ASPECT);
#if 0
  }
#endif
} // void Bild::set_window_restrictions()
