#pragma once

#include <gtkmm/messagedialog.h>

/** das Hilfefenster
 **/
class Hilfe : public Gtk::MessageDialog {
public:
  // Konstruktor
  Hilfe();

private:
  // initializiere das Hilfefenster
  void init();
}; // class Hilfe : public Gtk::MessageDialog

extern unique_ptr<Hilfe> hilfe;
