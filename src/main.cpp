#include "constants.h"
#include "bild.h"
#include "pfad.h"
#include "getopt/getopt.h"

#include <gtkmm/main.h>
#include <vector>
#include <sys/inotify.h>
#include <sys/ioctl.h>

int
main(int argc, char* argv[])
{
  //auto app = Gtk::Application::create("dknof.siv", Gio::APPLICATION_HANDLES_OPEN);
  auto bilder = vector<std::pair<unique_ptr<Bild>, int>>();

  if (argc == 1) {
#include "hilfe.string"
    cout << hilfe_string;
    return EXIT_SUCCESS;
  }

  inotify_fd = inotify_init();
  if (inotify_fd < 0) {
    cerr << "Konnte inotify nicht initialisieren, breche ab\n";
    return inotify_fd;
  }

  Gtk::Main kit;

  { // Kommandozeilenargumente durchgehen
    GetOpt::Option option;
    while ((option =
            GetOpt::getopt(argc, argv,
                           {{"hilfe",   'h',  GetOpt::Syntax::BOOL},
                           {"help",     '?',  GetOpt::Syntax::BOOL},
                           {"version",  'v',  GetOpt::Syntax::BOOL},
                           {"license",  'L',  GetOpt::Syntax::BOOL},
                           {"lizenz",   '\0', GetOpt::Syntax::BOOL},
                           }) // { }
           )) {
      if (option.fail()) {
        // Fehlbedienung
        cerr << argv[0] << "\n"
          << "Falsche Bedienung\n"
          << option.error() << " " << option.value_string() << "\n"
          << "Für Hilfe: '" << argv[0] << " --hilfe'"
          << '\n';
      } // if (option.fail())

      if (option.name() == "hilfe") {
        // Hilfe ausgeben
#include "hilfe.string"
        cout << hilfe_string;
      } else if (option.name() == "version") {
        // Ausgabe der Version
        cout << "sophie \n"
          << "Kompiliert: " << __DATE__ << ", " << __TIME__ << '\n'
          << "Kompiler: "
#if   defined(__GNUG__)
          << "g++"
#elif defined(__clang__)
          << "clang"
#elif defined(__ICC)
          << "icc"
#elif defined(__MSC_VER)
          << "msc"
#endif
          << '\n'
#ifdef __VERSION__
          << "  version: " << __VERSION__ << '\n'
#endif
#ifdef __cplusplus
          << "  C++ version: " << __cplusplus << '\n'
#endif
          ;
      } else if (   (option.name() == "license")
                 || (option.name() == "lizenz") ) {
        // output of the license (GPL)
#include "gpl.string"
        cout << "siv -- Lizenz:\n\n" << GPL_string;
      } else if (option.name().empty()) {
        // Datei angeben
        try {
          Pfad const pfad = option.value_string();
          unique_ptr<Bild> bild;
          try {
            bild = make_unique<Bild>(pfad);
          } catch(...) {
            if (pfad.ist_datei())
              throw "Konnte das Bild „" + pfad + "“ nicht öffnen.";
            else if (pfad.ist_verzeichnis())
              throw "Konnte im Verzeichnis „" + pfad + "“ kein Bild finden.";
            else
              throw "Unbekannter Pfad „" + pfad + "“.";
          }
          bild->show();
          // Dateiüberwachung einrichten
          auto wd = bild->pfad.erzeuge_inotify_beobachter();
          bilder.push_back({std::move(bild), wd});
        } catch (string meldung) {
          auto const e = errno;
          cerr << "Fehler: " << meldung << '\n';
          if (e)
            cerr << "Fehlercode: " << strerror(e) << '\n';
        }
      }
    }
  } // Kommandozeilenargumente durchgehen

  while (!bilder.empty()) {
    // Alle GTK-Ereignisse abarbeiten
    while(kit.events_pending())
      kit.iteration();

    { // geschlossene Fenster entfernen
      for (auto b = bilder.begin(); b != bilder.end(); ) {
        if (!b->first->is_visible()) {
          b = bilder.erase(b);
        } else {
          ++b;
        }
      }
    } // geschlossene Fenster entfernen

    { // inotify-Ereignisse abarbeiten
      static int bytes;
      ioctl(::inotify_fd, FIONREAD, &bytes);
      while (bytes > 0) {
        constexpr static auto BUF_LEN = sizeof(struct inotify_event) + NAME_MAX + 1; 
        static char buffer[BUF_LEN];
        auto num_read = read(inotify_fd, buffer, BUF_LEN);
        for (auto p = buffer; p < buffer + num_read; ) {
          auto event = reinterpret_cast<inotify_event*>(p);
          //cout << *event << '\n';
          for (auto& b : bilder) {
            auto& bild = b.first;
            auto& wd = b.second;
            if (wd != event->wd)
              continue ;
            try {
              bild->reload();
            } catch(...) {
              //cerr << "Konnte das Bild „" << bild->pfad << "“ nicht neu laden.\n";
              //break;
            }
            // Wenn die Datei gelöscht und neu angelegt wird, dann muss ein neuer Beobachter erstellt werden.
            inotify_rm_watch(inotify_fd, wd);
            wd = bild->pfad.erzeuge_inotify_beobachter();
          } // for (b : bilder)
          p += sizeof(inotify_event) + event->len;
        }
        ioctl(inotify_fd, FIONREAD, &bytes);
      } // while (bytes > 0)
    } // inotify-Ereignisse abarbeiten

    { // fehlende inotify watches erzeugen
      for (auto b = bilder.begin(); b != bilder.end(); ++b) {
        auto& bild = b->first;
        auto& wd = b->second;
        if (wd < 0) {
          wd = bild->pfad.erzeuge_inotify_beobachter();
          if (wd >= 0) {
            bild->reload();
          }
        }
      }
    } // fehlende inotify watches erzeugen 

    // Die Aktualisierung der Fenster wird nicht gleich vorgenommen sondern erst hier. Damit verbessert sich die Performance bei der Veränderung der Fenstergröße
    for (auto& b : bilder)
      b.first->update_if_outdated();

    // Pause
    usleep(10 * 1000);
  } // while (!bilder.empty())

  return EXIT_SUCCESS;
} // int main(int argc, char* argv[])
