#include "constants.h"
#include "ueber.h"

unique_ptr<Ueber> ueber;

/**
 ** Konstruktor
 ** 
 ** @param     -
 **
 ** @return    -
 **/
Ueber::Ueber() :
  Gtk::AboutDialog()
{
  this->signal_realize().connect(sigc::mem_fun(*this, &Ueber::init));
  this->signal_response().connect(sigc::hide(sigc::mem_fun(*this, &Ueber::hide)));
} // Ueber::Ueber()

/**
 ** initializiere das UI
 ** 
 ** @param     -
 **
 ** @return    -
 **/
void
Ueber::init()
{
  this->set_program_name("siv");
  //this->set_version(__DATE__);
  this->set_comments("Ein einfacher Bildbetrachter, der die Bilder automatisch bei Änderungen aktualisiert.");
  this->set_license_type(Gtk::LICENSE_GPL_3_0);
  this->set_website("https://gitlab.com/dknof/siv");
  this->set_authors({"Dr. Diether Knof <dknof@gmx.de>"});

  return ;
} // Ueber::init()
