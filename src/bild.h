#pragma once

#include "pfad.h"
#include <gtkmm/window.h>
namespace Gtk {
class DrawingArea;
};

class Bild : public Gtk::Window {
  public:
    Bild(Pfad pfad);
    ~Bild() = default;

    void update_if_outdated();
    void update();
    bool draw(Cairo::RefPtr<::Cairo::Context> cr);

    void clear();
    void reload();
    void auto_scale();
    void auto_scale_fit_screen();
    void set_scale(double scale, bool resize = false);

  private:
    bool on_key_press_event(GdkEventKey* key);
    bool on_scroll_event(GdkEventScroll* scroll);
    bool on_configure_event(GdkEventConfigure* event);

    void set_window_restrictions();

    Pfad neustes_bild() const;

  public:
    Pfad const pfad; // Datei oder Verzeichnis
  private:
    Gtk::DrawingArea* image = nullptr;
    Glib::RefPtr<Gdk::Pixbuf> pixbuf;
    double scale = 1;
    bool outdated = true;
}; // class Bild : public Gtk::Image
