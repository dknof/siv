#pragma once

#include <iostream>
#include <string>
using namespace std;

#define CLOG cerr << __FILE__ << '#' << __LINE__ << ' '
#define ABORT abort()
