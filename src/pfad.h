#pragma once

/** Ein Pfad zu einer Datei oder einem Verzeichnis
 **/
class Pfad : public string {
public:
  Pfad() = default;
  Pfad(string const& pfad);

  operator bool() const;

  string verzeichnis() const;
  string datei() const;
  string erweiterung() const;
  bool ist_datei() const;
  bool ist_verzeichnis() const;
  bool ist_bild() const;

  Pfad neustes_bild() const;
  int erzeuge_inotify_beobachter() const;

private:
}; // class Pfad : public string

extern int inotify_fd;
