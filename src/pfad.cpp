#include "constants.h"
#include "pfad.h"
#include <string.h>
#include <sys/stat.h>
#include <vector>
#include <sys/inotify.h>
#include <sys/ioctl.h>
#include <dirent.h>

int inotify_fd = 0;

bool operator<(timespec const& lhs, timespec const& rhs);
ostream& operator<<(ostream& ostr, inotify_event const& event);


/** Konstruktor
 ** 
 ** @param   pfad   Pfad
 **/
Pfad::Pfad(string const& pfad) :
  string(pfad)
{ }

/** Ob der Pfad leer ist
 **/
Pfad::operator bool() const
{
  return !this->empty();
} // Pfad::operator bool() const

/** -> Rückgabe
 **
 ** @param    -
 **
 ** @return   der Verzeichnisanteil
 **/
string
Pfad::verzeichnis() const
{
  if (this->ist_verzeichnis())
    return *this;
  auto n = this->find_last_of('/');
  if (n == string::npos)
    return ""s;
  return string(*this, 0, n);
} // string Pfad::verzeichnis() const

/** -> Rückgabe
 **
 ** @param    -
 **
 ** @return   die Datei
 **/
string
Pfad::datei() const
{
  if (this->ist_verzeichnis())
    return ""s;
  auto n = this->find_last_of('/');
  if (n == string::npos)
    return *this;
  return string(*this, n + 1, string::npos);
} // string Pfad::datei() const

/** -> Rückgabe
 **
 ** @param    -
 **
 ** @return   die Dateierweiterung
 **/
string
Pfad::erweiterung() const
{
  if (!this->ist_datei())
    return ""s;
  // Suche bekannte Dateiendungen
  auto const datei = this->datei();
  auto n = datei.find_last_of('.');
  if (n == string::npos)
    return ""s;
  return string(datei, n + 1, string::npos);
} // string Pfad::erweiterung() const

/** -> Rückgabe
 **
 ** @param    -
 **
 ** @return   ob es sich um eine reguläre Datei handelt
 **/
bool
Pfad::ist_bild() const
{
  if (!this->ist_datei())
    return false;
  auto const erweiterung = this->erweiterung();
  if (erweiterung.empty())
    return false;
  vector<char const*> const bild_extensions = {"jpg", "JPG", "jpeg", "JPEG", "png", "gif", "bmp", "pbm"};
  for (auto const e : bild_extensions)
    if (e == erweiterung)
      return true;
  return false;
} // bool ist_bild() const

/** -> Rückgabe
 **
 ** @param    -
 **
 ** @return   ob es sich um eine reguläre Datei handelt
 **/
bool
Pfad::ist_datei() const
{
  struct stat statbuf;
  if (stat(this->c_str(), &statbuf))
    return false;
  return S_ISREG(statbuf.st_mode);
} // bool Pfad::ist_datei() const

/** -> Rückgabe
 **
 ** @param    -
 **
 ** @return   ob es sich um eine reguläre Datei handelt
 **/
bool
Pfad::ist_verzeichnis() const
{
  struct stat statbuf;
  if (stat(this->c_str(), &statbuf))
    return false;
  return S_ISDIR(statbuf.st_mode);
} // bool Pfad::ist_verzeichnis() const

/** -> Rückgabe
 **
 ** @param    -
 **
 ** @return   das neuste Bild
 **/
Pfad
Pfad::neustes_bild() const
{
  if (this->ist_datei())
    return *this;
  if (!this->ist_verzeichnis())
    return {};

  // Neustes Bild aus dem Verzeichnis
  string const dir = *this + (this->back() == '/' ? "" : "/");
  struct dirent *dp;
  DIR *dfd = opendir(this->c_str());
  if (!dfd)
    return {};
  Pfad result;
  timespec result_ts;

  // Alle Verzeichniseinträge durchgehen
  struct stat statbuf;
  while ((dp = readdir(dfd))) {
    Pfad const pfad = dir + dp->d_name;
    if (stat(pfad.c_str(), &statbuf) == -1)
      continue;
    if (!S_ISREG(statbuf.st_mode))
      continue;
    if (   (!result || (result_ts < statbuf.st_mtim)) 
	&& pfad.ist_bild()) {
      result = pfad;
      result_ts = statbuf.st_mtim;
    }
  } // while ((dp = readdir(dfd)))

  return result;
} // Pfad Pfad::neustes_bild() const

/** Erstellt einen inotify-Beobachter
 **
 ** @param    -
 **
 ** @return   id des Beobachters
 **/
int
Pfad::erzeuge_inotify_beobachter() const
{
  if (this->ist_datei())
    return inotify_add_watch(inotify_fd, this->c_str(),
			     IN_CLOSE_WRITE
			     | IN_DELETE_SELF
			     | IN_MODIFY
			     | IN_MOVE_SELF);
  if (this->ist_verzeichnis())
    return inotify_add_watch(inotify_fd, this->c_str(),
			     IN_CREATE
			     | IN_CLOSE_WRITE
			     | IN_DELETE
			     | IN_DELETE_SELF
			     | IN_MOVE_SELF
			     | IN_MOVED_FROM
			     | IN_MOVED_TO
			    );
  return -1;
} // int Pfad::erzeuge_inotify_beobachter() const


bool
operator<(timespec const& lhs, timespec const& rhs)
{
  if (lhs.tv_sec == rhs.tv_sec)
    return lhs.tv_nsec < rhs.tv_nsec;
  else
    return lhs.tv_sec < rhs.tv_sec;
}

/**
 ** Gibt den inotify-event aus
 **/
ostream& operator<<(ostream& ostr, inotify_event const& event)
{
  ostr << "wd = " << event.wd;

  ostr << ", mask = ";
  if (event.mask & IN_ACCESS)        ostr << "IN_ACCESS ";
  if (event.mask & IN_ATTRIB)        ostr << "IN_ATTRIB ";
  if (event.mask & IN_CLOSE_NOWRITE) ostr << "IN_CLOSE_NOWRITE ";
  if (event.mask & IN_CLOSE_WRITE)   ostr << "IN_CLOSE_WRITE ";
  if (event.mask & IN_CREATE)        ostr << "IN_CREATE ";
  if (event.mask & IN_DELETE)        ostr << "IN_DELETE ";
  if (event.mask & IN_DELETE_SELF)   ostr << "IN_DELETE_SELF ";
  if (event.mask & IN_IGNORED)       ostr << "IN_IGNORED ";
  if (event.mask & IN_ISDIR)         ostr << "IN_ISDIR ";
  if (event.mask & IN_MODIFY)        ostr << "IN_MODIFY ";
  if (event.mask & IN_MOVE_SELF)     ostr << "IN_MOVE_SELF ";
  if (event.mask & IN_MOVED_FROM)    ostr << "IN_MOVED_FROM ";
  if (event.mask & IN_MOVED_TO)      ostr << "IN_MOVED_TO ";
  if (event.mask & IN_OPEN)          ostr << "IN_OPEN ";
  if (event.mask & IN_Q_OVERFLOW)    ostr << "IN_Q_OVERFLOW ";
  if (event.mask & IN_UNMOUNT)       ostr << "IN_UNMOUNT ";

  if (event.len > 0)
    ostr << ", name = " << event.name;

  return ostr;
} // ostream& operator<<(ostream& ostr, inotify_event event)
