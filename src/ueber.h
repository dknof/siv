#pragma once

#include <gtkmm/aboutdialog.h>

/** das Über-Fenster
 **/
class Ueber : public Gtk::AboutDialog {
public:
  // Konstruktor
  Ueber();

private:
  // initializiere das Über-Fenster
  void init();
}; // class Ueber : public Gtk::AboutDialog

extern unique_ptr<Ueber> ueber;
